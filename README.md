# Paraphraser

See @catasaurus's notes [here](https//gitlab.com/tangibleai/team/-/blob/main/project-reports/reports/2022-sebastian-notes.md)

Skip to the Get started [section](#get-started) if you know what you're doing and have a python virtual environment set up. 

## Tree

```text
├── code
│   └── in_progress
│       ├── incomplete.py
│       ├── ensemble.py
│       ├── seb.py
├── environment.yml
├── notebooks
│   └── in_progress
│       ├── bart_finetune_from_example.ipynb
│       ├── gpt2_finetune.ipynb
│       └── hobs_bart_finetune.ipynb
├── README.md
├── models 
│   ├── BART
│   │   └── outputs
│   │       └── best_model
│   │           ├── config.json
│   │           ├── eval_results.txt
│   │           ├── merges.txt
│   │           ├── model_args.json
│   │           ├── optimizer.pt
│   │           ├── pytorch_model.bin
│   │           ├── scheduler.pt
│   │           ├── special_tokens_map.json
│   │           ├── tokenizer.json
│   │           ├── tokenizer_config.json
│   │           ├── training_args.bin
│   │           └── vocab.json
│   └── BART2
│       └── outputs
│           ├── eval_results.txt
│           ├── training_progres_scores.csv
│           └── best_model
│               ├── config.json
│               ├── eval_results.txt
│               ├── merges.txt
│               ├── model_args.json
│               ├── optimizer.pt
│               ├── pytorch_model.bin
│               ├── scheduler.pt
│               ├── special_tokens_map.json
│               ├── tokenizer.json
│               ├── tokenizer_config.json
│               ├── training_args.bin
│               └── vocab.json
│
└── requirements.txt
```

## Create a virtual environment

In a `git-bash` or `bash-like` shell:

```bash
$ git clone git@gitlab.com:tangibleai/paraphraser
$ cd paraphraser
$ conda create -n paraphraser 'python=3.9.7'
$ conda activate paraphraser
```

Use *EITHER* `pip` *OR* `conda` to install the dependencies.

### Conda

Conda can install the dependencies on almost any platform where you've installed Anaconda:

```bash
$ conda env update -n paraphraser -f environment.yml
```

### Pip

If you prefer `pip` this may work, but do *NOT* mix `pip install` and `conda install|update`

```bash
pip install -r requirements.txt
```

## Get Started

Read the project report at https://gitlab.com/catasaurus/team/-/blob/main/project-reports/reports/sebastian_final_report_summer_2022.md

Or load the notebook used for training the model:
```bash
$ jupyter notebook
$ firefox http://localhost:8888/notebooks/notebooks/in_progress/bart_finetune_from_example.ipynb
```
