import torch
from sentence_tranformers import SentenceTransformer
from sentence_transformers.util import cos_sim

class EnsembleModel():
    def __init__(bart_path, t5_path, input_sentence model='all-mpnet-base-v2'):
        self.model = SentenceTransformer(model)
        self.bart = torch.load(bart_path)
        self.t5 = torch.load(t5_path)
        self.input_sentence = input_sentence

    def cos_score(input_sentence, out_sentence):
        sentences = [input_sentence, out_sentence]
        sentence_tensor = model.encode(sentences, convert_to_tensors=True)
        score = cos_sim(sentence_tensor[0], sentence_tensor[1])
        return score

    def diff_score(input_sentence, out_sentence):
        score = 0
        for i, j in enumerate(input_sentence):
            if out_sentence[i] == j:
                score -= 1
            else:
                score += 1
        return score

    def predict(input_sentence):
        bart = bart.predict([input_sentence])
        t5 = t5.predict([input_sentence])
        preds = bart + t5
        cos_scores = {}
        for i in preds:
            cos_scores[cos_score(input_sentence, i)] = i
        diff_scores = {}
        for i in preds:
            diff_scores[diff_score(input_sentence, i)] = i
        total_scores = {}
        for a, b in zip(cos_scores, diff_scores):
            total_scores[a+b] = cos_scores[a]
        return total_scores[max(total_scores)]


