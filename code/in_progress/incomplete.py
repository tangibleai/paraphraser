import tensorflow as tf
import pandas as pd
import numpy as np
import tensorflow_hub as hub
from tensorflow import keras
from tensorflow.keras import layers
from sentence_transformers import SentenceTransformer, util
import itertools
import datasets

dataset = datasets.load_dataset('wikitext', 'wikitext-103-v1')
for i, j in enumerate(dataset['train']):
	if len(j) < 50:
		del dataset['train'][i]
for i, j in enumerate(dataset['test']):
	if len(j) < 50:
		del dataset['test'][i]
for i, j in enumerate(dataset['validation']):
	if len(j) < 50:
		del dataset['validation'][i]

loss_fn_model = SentenceTransformer('paraphrase-multilingual-mpnet-base-v2')
def sentence_similarity(input_, out, transformer):
	sentence_embeddings = transformer.encode([input_, out])
	return util.cos_sim(sentence_embeddings[0], sentence_embeddings[1])

def sentence_diff(input_, out):
	return len([b for a, b in itertools.zip_longest(input_.split(), out.split()) if a != b])

def loss_fn(input_, out, model):
	similarity  = sentence_similarity(input_, out, model)
	diff = sentence_diff(input_ out)
	if diff >= 1:
		return similarity
	else:
		return -1

class BahdanauAttention(tf.keras.layers.Layer):
  def __init__(self, units):
    super().__init__()
    self.W1 = tf.keras.layers.Dense(units, use_bias=False)
    self.W2 = tf.keras.layers.Dense(units, use_bias=False)

    self.attention = tf.keras.layers.AdditiveAttention()

  def call(self, query, value, mask):
    shape_checker = ShapeChecker()
    shape_checker(query, ('batch', 't', 'query_units'))
    shape_checker(value, ('batch', 's', 'value_units'))
    shape_checker(mask, ('batch', 's'))

    w1_query = self.W1(query)
    shape_checker(w1_query, ('batch', 't', 'attn_units'))

    w2_key = self.W2(value)
    shape_checker(w2_key, ('batch', 's', 'attn_units'))

    query_mask = tf.ones(tf.shape(query)[:-1], dtype=bool)
    value_mask = mask

    context_vector, attention_weights = self.attention(
        inputs = [w1_query, value, w2_key],
        mask=[query_mask, value_mask],
        return_attention_scores = True,
    )
    shape_checker(context_vector, ('batch', 't', 'value_units'))
    shape_checker(attention_weights, ('batch', 't', 's'))

    return context_vector, attention_weights

class Encoder(tf.keras.layers.Layer):
	def __init__(self, input_vocab_size, embedding_dim, enc_units):
		super(Encoder, self).__init__()
	    self.enc_units = enc_units
	    self.input_vocab_size = input_vocab_size
	    self.embedding = tf.keras.layers.Embedding(self.input_vocab_size,
	                                               embedding_dim)
	    self.gru = tf.keras.layers.GRU(self.enc_units,
	                                   return_sequences=True,
	                                   return_state=True,
	                                   recurrent_initializer='glorot_uniform')

	  	def call(self, tokens, state=None):
		    shape_checker = ShapeChecker()
		    shape_checker(tokens, ('batch', 's'))
		    vectors = self.embedding(tokens)
		    shape_checker(vectors, ('batch', 's', 'embed_dim'))
		    output, state = self.gru(vectors, initial_state=state)
		    shape_checker(output, ('batch', 's', 'enc_units'))
		    shape_checker(state, ('batch', 'enc_units'))
		    return output, state
