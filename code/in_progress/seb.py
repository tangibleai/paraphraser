class Metric():
  """
  make sure that all get_* functions take a pair
  make sure that all get_best_* functions take a text and a list of strings
  make sure that all distance calculations = 1 - similarity
  make sure that best similarity functions use max() of similarities
  """
  def __init__(self, embedding, cos_sim, edit_weight=0.3, min_edit_distance=5, max_edit_distance=10):
    """
    __init__ function to initialize Metric class.

    Args:
      embedding: Embedding model to use for cosine similarity and distance.
      cos_sim: Function to measure cosine similarity between two embeddings.
      edit_weight: The weight assigned to the edit distance for the weighted average in compute_metric. The weight for cosine similarity is found by taking 1 - edit_weight.
      min_edit_distance: The minimum edit distance that can be found when taking the minimum edit distance with the prediction (text) and every true paraphrase (texts).
      max_edit_distance: Like the min_edit_distance except the maximum instead of the minimum.

    Returns:
      None as this is an __init__ function.
    """
    self.embedding = embedding
    self.cos_sim = cos_sim
    self.edit_weight = edit_weight
    self.min_edit_distance_ = min_edit_distance
    self.max_edit_distance_ = max_edit_distance

  def get_max_edit_distance(self, text, texts):
    """
    Returns the maximum edit distance found by finding the edit distance of every predicted paraphrase (text) and item in list of true paraphrases (texts).

    Args:
      text (str): The predicted paraphrase
      texts (list of str): A list of true paraphrases

    Returns:
      The minimum edit distance found as explained above (int).
    """
    distances = [self.get_edit_distance(text, s) for s in texts]

    return max(distances)
  
  def get_min_edit_distance(self, text, texts):
    """
    Returns the minimum edit distance found by finding the edit distance of every predicted paraphrase (text) and item in list of true paraphrases (texts).

    Args:
      text (str): The predicted paraphrase.
      texts (list of str): A list of true paraphrases.
    
    Returns:
      The minimum edit distance found as explained above (int).
    """
    distances = [self.get_edit_distance(text, s) for s in texts]
    
    return min(distances)

  def get_edit_distance(self, input_sentence, output_sentence):
    """
    Returns the edit distance (the number of insertions and deletions required to turn one text into another) between two texts.

    Args:
      input_sentence (str): The first of the two sentences used to measure edit distance.
      output_sentence (str): The second of the two sentences used to measure edit distance.
    
    Returns:
      The edit distance found between two strings (str).
    """
    if len(input_sentence) > len(output_sentence):
        input_sentence, output_sentence = input_sentence, output_sentence

    distances = range(len(input_sentence) + 1)
    for i2, c2 in enumerate(output_sentence):
        distances_ = [i2+1]
        for i1, c1 in enumerate(input_sentence):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]
  
  def get_best_cos_sim(self, text, other_texts):
    """
    Returns the minimum cosine similarity found between a list of pairs of text and a true paraphrase in texts.

    Args:
      text (str): The predicted sentence to be used to measure cosine similarity.
      other_texts (list of str): A list of true paraphrases.

    Returns:
      A float representing the minimum cosine similarity found (float).
    """
    return min([self.get_cos_sim(text, s) for s in other_texts])
  
  def get_cos_sim(self, text_a, text_b):
    """
    Find the cosine similarity between two texts.

    Args:
      text_a: One of the two sentences that the cosine similarity will be measured for.
      text_b: The second of the two sentences that the cosine similarity will be measured for.

    Returns:
      The cosine similarity found (float).
    """
    return self.cos_sim(self.embedding.encode(text_a, convert_to_tensor=True), self.embedding.encode(text_b, convert_to_tensor=True))
  
  def get_cos_dist(self, text_a, text_b):
    """
    Finds the cosine distance between two texts (cosine distance is the opposite of cosine similarity).

    Args:
      text_a: The first of the two sentences that the cosine distance will be measured for.
      text_b: The second of the two sentences that the cosine distance will be measured for.
    
    Returns:
      The cosine distance found between the two texts (float).
    """
    return 1 - self.get_cos_sim(text_a, text_b)
  
  def get_best_cos_dist(self, text, other_texts):
    """
    Returns the max cosine distance found between pairs of a predicted text and a item from a list of true paraphrases.

    Args:
      text: The predicted text.
      texts: The list of true paraphrases.

    Returns:
      The maximum cosine distance found (float)
    """
    return max([self.get_cos_dist(text, s) for s in other_texts])
  
  def get_best_edit_distance(self, pred, true_paraphrases):
    """
    Returns the minimum edit distance found between pairs of a predicted paraphrase and item from a list of true paraphrases.

    Args:
      pred: The predicted paraphrase.
      true_paraphrases: A list of true paraphrases.

    Returns:
      The minimum edit distance found (float).
    """
    return min([self.get_edit_distance(pred, truth) / max(len(pred), len(truth)) for truth in true_paraphrases])
  
  def compute_metric(self, pred, true_paraphrases):
    
    print('pred')
    print(pred)
    print('true_paraphrases')
    print(true_paraphrases)

    min_edit_distance = self.get_min_edit_distance(pred, true_paraphrases)

    print('min_edit_distance')
    print(min_edit_distance)

    if min_edit_distance < self.min_edit_distance_:
      return 0.
    complete_edit_distance = min_edit_distance - self.min_edit_distance_

    print('complete_edit_distance')
    print(complete_edit_distance)

    norm_edit_distance = complete_edit_distance / max(len(s) for s in list(true_paraphrases) + [pred])

    print('norm_edit_distance')
    print(complete_edit_distance)

    edit_distance = self.get_best_edit_distance(pred, true_paraphrases)
    max_len = max([len(s) for s in true_paraphrases+[pred]])
    
    """
    complete_edit_distance = np.abs(
            edit_distance
            - min_edit_distance
            ) / max_len
    cos_dist = float(self.get_best_cos_dist(pred, true_paraphrases))
    """

    
    print('c')
    print(complete_edit_distance)
    print('d')
    print(cos_dist)
    print('m')
    print(min_edit_distance)
    print('e')
    print(edit_distance)

    return (complete_edit_distance * self.edit_weight) + (cos_dist * (1 - self.edit_weight))

  def compute_edit_distance_metric(self, text, texts):
        
    print('pred')
    print(text)
    print('true_paraphrases')
    print(texts)

    min_edit_distance = self.get_min_edit_distance(text, texts)

    print('min_edit_distance')
    print(min_edit_distance)

    if min_edit_distance < self.min_edit_distance_:
      return 0.

    max_edit_distance = self.get_max_edit_distance(text, texts)

    print('max_edit_distance')
    print(max_edit_distance)

    if max_edit_distance > self.max_edit_distance_:
      return 0

    complete_edit_distance = min_edit_distance - self.min_edit_distance_

    print('complete_edit_distance')
    print(complete_edit_distance)

    norm_edit_distance = complete_edit_distance / max(len(s) for s in list(texts) + [text])

    print('norm_edit_distance')
    print(norm_edit_distance)

    return norm_edit_distance
