## Training

1. Train the model on a dataset that includes multiple different correct paraphrase wordings. Does that reduce or improve the accuracy on your test set?
2. Make sure your test dataset does not include any source sentences that are in your training dataset. Because you have duplicates you can no longer do a normal random `.sample()` of the entire dataset.

## Metric

1. Efficiently find all the "true" paraphrases for a particular sentence within the dataset so that you can compute your metric on a test set of paraphrases that includes some duplicate source_sentences.
2. Penalize your metric (accuracy score) if the minimum distance is too close to zero (didn't significantly change the sentence wording). Create a variable `self.min_edit_distance` and set it to a default of 5
3. Create a function `get_min_edit_distance(sentences): return max(.1 * min([len(s) for s in sentences]), self.min_edit_distance)` to make sure the min_edit_distance is a reasonable value for each set of sentence.
4. Modify the compute_metric() function to look somthing like this, so that 0 edit distance doesn't incentivize paraphrases that are the same as the input sentence:

```python
def compute_metric(self, pred, true_paraphrases):
    return (
        np.abs(
            self.best_edit_distance(pred, true_paraphrases)
            - self.get_min_edit_distance(
                [pred] + list(true_paraphrases)
                )
        )
    + self.get_cos_sim(pred, true_paraphrases)
    ) / 2
```
